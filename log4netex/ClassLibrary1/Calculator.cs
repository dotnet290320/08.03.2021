﻿using log4net;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public static class Calculator
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public static void Add(int x, int y)
        {
            log.Info($"Calculator {x} {y}");
        }
    }
}
