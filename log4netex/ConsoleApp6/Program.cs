﻿using ClassLibrary1;
using log4net;
using log4net.Config;
using System    ;
using System.IO;
using System.Reflection;

namespace ConsoleApp6
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            log.Info("Hello logging world!");

            Calculator.Add(3, 4);

            Console.WriteLine("Hit enter");
            Console.ReadLine();
        }
    }
}
